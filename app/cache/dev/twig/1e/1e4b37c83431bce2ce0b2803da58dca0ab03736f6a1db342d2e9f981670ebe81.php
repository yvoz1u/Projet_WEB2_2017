<?php

/* registration/register.html.twig */
class __TwigTemplate_1b752f3aa2879c3ff06716c6ce6baae37637e1a62c094e552564f95ea8e09de4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e7b51d0e09aee26e4373242975823ae21a6a5fba78ad6e0efbb03e004154b4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e7b51d0e09aee26e4373242975823ae21a6a5fba78ad6e0efbb03e004154b4b->enter($__internal_4e7b51d0e09aee26e4373242975823ae21a6a5fba78ad6e0efbb03e004154b4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "registration/register.html.twig"));

        // line 1
        echo "<html>
  <head>
    <style>
      body{
      background:url(\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/deathstar.png"), "html", null, true);
        echo "\");
      background-repeat: no-repeat;
      background-size:cover;
      color:green;
      }
      #inscription{
      text-align:center;
      text-decoration:underline;
      color:yellow;
      }
      #form{
      text-align:center;
      }
    </style>
    </head>
   
<body>

<div id='inscription'><h1>INSCRIPTION</h1></div></br>

<div id='form'>
";
        // line 26
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'row');
        echo "</br>
    ";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'row');
        echo "</br>
    ";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'row');
        echo "</br>
    ";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'row');
        echo "</br>

    <button type=\"submit\">Register!</button>
";
        // line 33
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
</div>



    </body>
</html>
";
        
        $__internal_4e7b51d0e09aee26e4373242975823ae21a6a5fba78ad6e0efbb03e004154b4b->leave($__internal_4e7b51d0e09aee26e4373242975823ae21a6a5fba78ad6e0efbb03e004154b4b_prof);

    }

    public function getTemplateName()
    {
        return "registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 33,  68 => 30,  64 => 29,  60 => 28,  56 => 27,  52 => 26,  28 => 5,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
  <head>
    <style>
      body{
      background:url(\"{{asset('imgs/deathstar.png')}}\");
      background-repeat: no-repeat;
      background-size:cover;
      color:green;
      }
      #inscription{
      text-align:center;
      text-decoration:underline;
      color:yellow;
      }
      #form{
      text-align:center;
      }
    </style>
    </head>
   
<body>

<div id='inscription'><h1>INSCRIPTION</h1></div></br>

<div id='form'>
{{ form_start(form) }}
    {{ form_row(form.username) }}</br>
    {{ form_row(form.email) }}</br>
    {{ form_row(form.plainPassword.first) }}</br>
    {{ form_row(form.plainPassword.second) }}</br>

    <button type=\"submit\">Register!</button>
{{ form_end(form) }}
</div>



    </body>
</html>
", "registration/register.html.twig", "/var/www/html/love_letter/Projet_WEB2_2017/app/Resources/views/registration/register.html.twig");
    }
}
