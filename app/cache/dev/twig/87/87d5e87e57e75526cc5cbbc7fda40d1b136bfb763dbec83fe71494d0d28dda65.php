<?php

/* security/login.html.twig */
class __TwigTemplate_0e93238fc7149eebe480b8e6e272d8ff04a1c62705ffd6259439b7e306e62ff1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_824637d5d7a36c8c3f9facbd67c23fe0d98a875baed78a1f4b3087bce07fe635 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_824637d5d7a36c8c3f9facbd67c23fe0d98a875baed78a1f4b3087bce07fe635->enter($__internal_824637d5d7a36c8c3f9facbd67c23fe0d98a875baed78a1f4b3087bce07fe635_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_824637d5d7a36c8c3f9facbd67c23fe0d98a875baed78a1f4b3087bce07fe635->leave($__internal_824637d5d7a36c8c3f9facbd67c23fe0d98a875baed78a1f4b3087bce07fe635_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_e115bcaf50b824869da49460c92f886ad47231c1a9879e27f8359a72720dc30a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e115bcaf50b824869da49460c92f886ad47231c1a9879e27f8359a72720dc30a->enter($__internal_e115bcaf50b824869da49460c92f886ad47231c1a9879e27f8359a72720dc30a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 4
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo "

<form action=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />

    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    <button type=\"submit\">login</button>
</form>
";
        
        $__internal_e115bcaf50b824869da49460c92f886ad47231c1a9879e27f8359a72720dc30a->leave($__internal_e115bcaf50b824869da49460c92f886ad47231c1a9879e27f8359a72720dc30a_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 10,  52 => 8,  48 => 6,  42 => 4,  40 => 3,  34 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body %}
{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}


<form action=\"{{ path('login') }}\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" />

    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    <button type=\"submit\">login</button>
</form>
{% endblock %}
", "security/login.html.twig", "/var/www/html/love_letter/Projet_WEB2_2017/app/Resources/views/security/login.html.twig");
    }
}
