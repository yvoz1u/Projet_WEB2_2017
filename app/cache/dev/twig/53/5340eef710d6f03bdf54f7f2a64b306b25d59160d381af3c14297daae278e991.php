<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_342f7b84678e72e3ec97ceb11a9b8d931890be31ded80da73b5657b694d4e41c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ecbe40a044258b9fcf4438797cdddcf08c794e05e818e0eea424f6fad63a0fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ecbe40a044258b9fcf4438797cdddcf08c794e05e818e0eea424f6fad63a0fe->enter($__internal_9ecbe40a044258b9fcf4438797cdddcf08c794e05e818e0eea424f6fad63a0fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ecbe40a044258b9fcf4438797cdddcf08c794e05e818e0eea424f6fad63a0fe->leave($__internal_9ecbe40a044258b9fcf4438797cdddcf08c794e05e818e0eea424f6fad63a0fe_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_1631a6f889afc499c2eff089bc39844c4e34b9990a385932629715ea56d662fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1631a6f889afc499c2eff089bc39844c4e34b9990a385932629715ea56d662fe->enter($__internal_1631a6f889afc499c2eff089bc39844c4e34b9990a385932629715ea56d662fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_1631a6f889afc499c2eff089bc39844c4e34b9990a385932629715ea56d662fe->leave($__internal_1631a6f889afc499c2eff089bc39844c4e34b9990a385932629715ea56d662fe_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e45962db9aafe2e0b82e1bc53b8547fc56e3e50d176cbfce5375e6726bd7255b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e45962db9aafe2e0b82e1bc53b8547fc56e3e50d176cbfce5375e6726bd7255b->enter($__internal_e45962db9aafe2e0b82e1bc53b8547fc56e3e50d176cbfce5375e6726bd7255b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e45962db9aafe2e0b82e1bc53b8547fc56e3e50d176cbfce5375e6726bd7255b->leave($__internal_e45962db9aafe2e0b82e1bc53b8547fc56e3e50d176cbfce5375e6726bd7255b_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d2f9e3e06aafd1c346a6bda0cac79e434e6078701f0566acd4a87bd8fc945731 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2f9e3e06aafd1c346a6bda0cac79e434e6078701f0566acd4a87bd8fc945731->enter($__internal_d2f9e3e06aafd1c346a6bda0cac79e434e6078701f0566acd4a87bd8fc945731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_d2f9e3e06aafd1c346a6bda0cac79e434e6078701f0566acd4a87bd8fc945731->leave($__internal_d2f9e3e06aafd1c346a6bda0cac79e434e6078701f0566acd4a87bd8fc945731_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/html/love_letter/Projet_WEB2_2017/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
