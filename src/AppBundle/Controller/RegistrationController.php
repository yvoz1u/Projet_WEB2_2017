<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use AppBundle\Entity\Main;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            // 4) Stop if the user already exist
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            'SELECT u
            FROM AppBundle:User u
            WHERE u.pseudo = :pseudo'
            )->setParameter('pseudo', $user->getUsername());

            $products = $query->getOneOrNullResult();

            if (is_null($products)) {
            // 5) save the User!
                
                $em->persist($user);
                $em->flush();
		//creer une nouvelle main pour le joueur
		$mainNouv = new Main();
		$mainNouv->setJoueur($user);
		$em->persist($mainNouv);
		$em->flush();

                return $this->redirectToRoute('app_accueil_niceaccueil');
            }
        }

        return $this->render(
            'registration/register.html.twig',
            array('form' => $form->createView())
        );
    }
}
?>
