<?php 

namespace AppBundle\Controller;

use AppBundle\Entity\Cartes;
use AppBundle\Entity\Pioche;
use AppBundle\Entity\Serveur;
use AppBundle\Entity\User;
use AppBundle\Entity\Main;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AffCarte extends Controller{

 /**
  *@Route("/affichercartes")
  */
	public function AfficherCartes(Request $request){
		//recuperer toutes les cartes dispo dans le jeu
		$em = $this->getDoctrine()->getManager();
		/*$query = $em->createQuery(
		    'SELECT c
		    FROM AppBundle:Cartes c'
		);
		$cartes = $query->getResult();
		foreach ($cartes as $c){
			echo 'carte num'.$c->getValeur().'  Carte nom'.$c->getNom().'\n';
		}*/
		
		// recupérer l'utilisateur	
		$pseudo = $this->get('security.context')->getToken()->getUser()->getUsername();
		echo $pseudo;	
		$query = $em->createQuery(
            		'SELECT u
            		FROM AppBundle:User u
            		WHERE u.pseudo = :pseudo'
            	)->setParameter('pseudo', $pseudo);

            	$joueur = $query->getOneOrNullResult();

            	if (!is_null($joueur)) {
			// choper le serv 1  et l'ajouter au joueur
			$query = $em->createQuery(
            		'SELECT u
            		FROM AppBundle:Serveur u
            		WHERE u.id = :id'
            		)->setParameter('id', 1);

            		$servSet = $query->getOneOrNullResult();

			$joueur->setServeur($servSet);
			// assigner un numéro au joueur
			$query = $em->createQuery(
            		'SELECT j
            		FROM AppBundle:User j
            		WHERE j.serveur = :s'
            		)->setParameter('s', $joueur->getServeur());
			$LisJoueur = $query->getResult();
			$nbJoueurs = count($LisJoueur);
			$joueur->setNumJoueur($nbJoueurs+1);
			echo 'Numéro du joueur:'.$joueur->getNumJoueur();	

			$em->flush();
			echo '\Serveur :'.$joueur->getServeur()->getId();
			// INITIALISATION DE LA PIOCHE DU SERVEUR
			for($i=1;$i<=8;$i++){
				$query = $em->createQuery(
            			'SELECT p
            			FROM AppBundle:Pioche p
            			WHERE p.serveur = :serveur AND p.valeur = :val' 
            			)->setParameters(array('serveur' => $joueur->getServeur()->getId(),'val'=>$i));
				$pioche = $query->getOneOrNullResult();
				if(is_null($pioche)){echo 'pioche null';}
				$query = $em->createQuery(
            			'SELECT c
            			FROM AppBundle:Cartes c
            			WHERE c.valeur = :valeur' 
            			)->setParameter('valeur',$i);
				$carte = $query->getOneOrNullResult();
				if(is_null($carte)){echo 'pioche null';}
				$pioche->setNbCopies($carte->getNbCopies());
				$em->flush();
			}
			//faire piocher aux joueurs
			//recuperer la main 
			$query = $em->createQuery(
	 	 	 'SELECT m
	 	 	 FROM AppBundle:Main m WHERE m.joueur=:j')->setParameter('j',$joueur->getId());
			$main= $query->getOneOrNullResult();
			//nbcartestotal
			$query = $em->createQuery(
			    'SELECT c
			    FROM AppBundle:Pioche c WHERE c.serveur = :serveur')->setParameter('serveur' , $joueur->getServeur()->getId());;
			$piocheact = $query->getResult();
			$cartesTot=0;
			foreach ($piocheact as $p){
				$cartesTot+=$p->getNbCopies();
			}
			//for
			$carteChoisie=rand(1,$cartesTot);
			$currentNum=0;
			foreach ($piocheact as $p){
				$currentNum+=$p->getNbCopies();
				echo $currentNum.'     ';
				//si dans intervalle choisi,puis decremanteNbCartes
				if($currentNum>=$carteChoisie){
					$p->setNbCopies($p->getNbCopies()-1);
					$main->setCarte1($p->getCarte());
					break;
				}
			}
			$em->flush();
					
			//si dans intervalle choisi,puis decremanteNbCartes
			
		}
	}
}
