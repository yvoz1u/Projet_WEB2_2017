<?php
namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class JeuController extends Controller
{
    /**
     * @Route("/jeu", name="jeu")
     */
    public function jeuAction(Request $request){
	// récupérer le joueur
    	/*$pseudo = $this->get('security.context')->getToken()->getUser()->getUsername();
	$query = $em->createQuery(
           	'SELECT u
            	FROM AppBundle:User u
            	WHERE u.pseudo = :pseudo'
        )->setParameter('pseudo', $pseudo);
	$joueur = $query->getOneOrNullResult();
	if($joueur->getServeur()!=1){
	// choper le serv 1  et l'ajouter au joueur
		$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Serveur u
            	WHERE u.id = :id'
            	)->setParameter('id', 1);

            	$servSet = $query->getOneOrNullResult();

		$joueur->setServeur($servSet);
		// assigner un numéro au joueur
		$query = $em->createQuery(
            	'SELECT j
            	FROM AppBundle:User j
            	WHERE j.serveur = :s'
            	)->setParameter('s', $joueur->getServeur());
		$LisJoueur = $query->getResult();
		$nbJoueurs = count($LisJoueur);
		$joueur->setNumJoueur($nbJoueurs+1);
		$em->flush();
		// faire piocher une carte
		//recuperer la main 
		$query = $em->createQuery(
	 	  'SELECT m
	 	  FROM AppBundle:Main m WHERE m.joueur=:j')->setParameter('j',$joueur->getId());
		$main= $query->getOneOrNullResult();
		//nbcartestotal
		$query = $em->createQuery(
		    'SELECT c
		    FROM AppBundle:Pioche c WHERE c.serveur = :serveur')->setParameter('serveur' , $joueur->getServeur()->getId());;
		$piocheact = $query->getResult();
		$cartesTot=0;
		foreach ($piocheact as $p){
			$cartesTot+=$p->getNbCopies();
		}
		//for
		$carteChoisie=rand(1,$cartesTot);
		$currentNum=0;
		foreach ($piocheact as $p){
			$currentNum+=$p->getNbCopies();
			//si dans intervalle choisi,puis decremanteNbCartes
			if($currentNum>=$carteChoisie){
				$p->setNbCopies($p->getNbCopies()-1);
				$main->setCarte1($p->getCarte());
				break;
			}
		}
		$em->flush();
	}
	// combien de joueurs sur le serveur
	$query = $em->createQuery(
           'SELECT j
           FROM AppBundle:User j
            WHERE j.serveur = :s'
        )->setParameter('s', $joueur->getServeur());
	$LisJoueur = $query->getResult();
	$nbJoueurs = count($LisJoueur);
	// tour du serveur
	$tourServ = $joueur->getServeur()->getNumTour();
	
	if($nbJoueurs==2 and $tourServ==0){
		$joueur->getServeur()->setNumTour(1);
		$em->flush();
	}
	if($joueur->getNumJoueur()==$joueur->getServeur()->getNumTour()){
		$tourJoueur=true;
		$query = $em->createQuery(
	 	  'SELECT m
	 	  FROM AppBundle:Main m WHERE m.joueur=:j')->setParameter('j',$joueur->getId());
		$main= $query->getOneOrNullResult();
		//vérifier nombre de cartes en main
		if(is_null($main->getCarte1()) || is_null($main->getCarte2())){
		
			// faire piocher
			//nbcartestotal
			$query = $em->createQuery(
			    'SELECT c
			    FROM AppBundle:Pioche c WHERE c.serveur = :serveur')->setParameter('serveur' , $joueur->getServeur()->getId());;
			$piocheact = $query->getResult();
			$cartesTot=0;
			foreach ($piocheact as $p){
				$cartesTot+=$p->getNbCopies();
			}
			//for
			$carteChoisie=rand(1,$cartesTot);
			$currentNum=0;
			foreach ($piocheact as $p){
				$currentNum+=$p->getNbCopies();
				//si dans intervalle choisi,puis decremanteNbCartes
				if($currentNum>=$carteChoisie){
					$p->setNbCopies($p->getNbCopies()-1);
					if(is_null($main->getCarte1())){
						$main->setCarte1($p->getCarte());
					}else{
						$main->setCarte2($p->getCarte());
					}
					break;
				}
			}
			$em->flush();
			
		
		}else{
		//Regarder quelle carte est jouée
			$carteJouee = $this->request->get('carte',0);
			if($carteJouee ==1){
				$main->setCarte1(NULL);
			}else{
				$main->setCarte2(NULL);
			}
			$em->flush();
		}
	}else{
		$tourJoueur=false;
		//dire à la vue d'attendre
	}*/

    return $this->render('index.html.php'/*, array(
        'carte1' => $lastUsername,
        'carte2' => $error,
	'tourJoueur' => ,
    )*/
);
    }  
}
?>
