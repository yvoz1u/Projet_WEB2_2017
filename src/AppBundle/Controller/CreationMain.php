<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Cartes;
use AppBundle\Entity\User;
use AppBundle\Entity\Main;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CreationMain extends Controller{

 /**
  *@Route("/balancetesmains")
  */
   public function CreateAction(){
	$em = $this->getDoctrine()->getManager();
	
	$query = $em->createQuery(
	   'SELECT u
	   FROM AppBundle:User u ');
	$users = $query->getResult();
	foreach($users as $u){
		$query = $em->createQuery(
	 	  'SELECT m
	 	  FROM AppBundle:Main m WHERE m.joueur=:j')->setParameter('j',$u->getUsername());
		$main= $query->getOneOrNullResult();
		if(is_null($main)){
			$mainNouv = new Main();
			$mainNouv->setJoueur($u);
			$em->persist($mainNouv);
			$em->flush();
		}
	}

	return new Response('done');
   }
}
?>
