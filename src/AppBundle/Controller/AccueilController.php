<?php 

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccueilController extends Controller{

 /**
  *@Route("/accueil")
  */
   public function niceAccueil(){

	$user = $this->get('security.token_storage')->getToken()->getUser();
	return $this->render(
            'accueil/Accueil_love_letter.html.twig');
   }
}
