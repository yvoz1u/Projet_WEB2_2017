<?php 

namespace AppBundle\Controller;

use AppBundle\Entity\Pioche;
use AppBundle\Entity\Cartes;
use AppBundle\Entity\Serveur;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CreationPioche extends Controller{

 /**
  *@Route("/balancetespioches")
  */
   public function CreateAction(){
	$em = $this->getDoctrine()->getManager();

	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Serveur u
          	WHERE u.id = :id'
        )->setParameter('id', 1);

        $servSet = $query->getOneOrNullResult();

	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 1);
	$car1 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 2);
	$car2 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 3);
	$car3 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 4);
	$car4 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 5);
	$car5 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 6);
	$car6 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 7);
	$car7 = $query->getOneOrNullResult();
	$query = $em->createQuery(
            	'SELECT u
            	FROM AppBundle:Cartes u
          	WHERE u.valeur = :valeur'
        )->setParameter('valeur', 8);
	$car8 = $query->getOneOrNullResult();

	$p1 = new Pioche();
	$p1->setServeur($servSet);
	$p1->setCarte($car1);
	$p1->setValeur(1);
	$em->persist($p1);

	$p2 = new Pioche();
	$p2->setServeur($servSet);
	$p2->setCarte($car2);
	$p2->setValeur(2);
	$em->persist($p2);

	$p3 = new Pioche();
	$p3->setServeur($servSet);
	$p3->setCarte($car3);
	$p3->setValeur(3);
	$em->persist($p3);

	$p4 = new Pioche();
	$p4->setServeur($servSet);
	$p4->setCarte($car4);
	$p4->setValeur(4);
	$em->persist($p4);

	$p5 = new Pioche();
	$p5->setServeur($servSet);
	$p5->setCarte($car5);
	$p5->setValeur(5);
	$em->persist($p5);

	$p6 = new Pioche();
	$p6->setServeur($servSet);
	$p6->setCarte($car6);
	$p6->setValeur(6);
	$em->persist($p6);

	$p7 = new Pioche();
	$p7->setServeur($servSet);
	$p7->setCarte($car7);
	$p7->setValeur(7);
	$em->persist($p7);

	$p8 = new Pioche();
	$p8->setServeur($servSet);
	$p8->setCarte($car8);
	$p8->setValeur(8);
	$em->persist($p8);


    	// actually executes the queries (i.e. the INSERT query)
    	$em->flush();
	

	return new Response('nice');
   }
}

