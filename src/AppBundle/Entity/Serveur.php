<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
*@ORM\Entity
*@ORM\Table(name="serveur")
*/
class Serveur{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
    /**
     *@ORM\Column(type="integer")
     */
	private $numTour; //1 pour le joueur 1, 2 pour le joueur 2...
    /**
     *@ORM\Column(type="integer")
     */
	private $numManche;
    /**
     * @ORM\ManyToOne(targetEntity="Cartes")
     * @ORM\JoinColumn(name="dercarte_id", referencedColumnName="id", nullable=true)
     */
	private $derCarte;

	public function getId(){
		return $this->id;
	}
	
	public function getNumTour(){
		return $this->numTour;
	}
	public function setNumTour($t){
		$this->numTour=$t;
	}
        public function getNumManche(){
		return $this->numManche;
	}
	public function setNumManche($m){
		$this->numManche=$m;
	}
	public function getDerCarte(){
		return $this->derCarte;
	}
	public function setDerCarte($m){
		$this->derCarte=$m;
	}
}
?>
