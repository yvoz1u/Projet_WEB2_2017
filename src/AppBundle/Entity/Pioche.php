<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
*@ORM\Entity
*@ORM\Table(name="pioche")
*/
class Pioche{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Serveur")
     * @ORM\JoinColumn(name="serveur_id", referencedColumnName="id")
     */
	private $serveur;
    /**
     * @ORM\ManyToOne(targetEntity="Cartes")
     * @ORM\JoinColumn(name="carte_id", referencedColumnName="id")
     */
	private $carte;
    /**
     * @ORM\Column(type="integer")
     */
	private $nbCopies=0;
     /**
     * @ORM\Column(type="integer")
     */
	private $valeur=0;

	public function getServeur(){
		return $this->serveur;
	}
	public function setServeur($s){
		$this->serveur=$s;
	}
	public function getCarte(){
		return $this->carte;
	}
	public function setCarte($s){
		$this->carte=$s;
	}
	public function getNbCopies(){
		return $this->nbCopies;
	}
	public function setNbCopies($s){
		$this->nbCopies=$s;
	}
	public function getValeur(){
		return $this->serveur;
	}
	public function setValeur($v){
		$this->valeur=$v;
	}
}
?>
