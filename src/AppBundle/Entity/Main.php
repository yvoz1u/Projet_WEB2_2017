<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
*@ORM\Entity
*@ORM\Table(name="main")
*/
class Main{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
	
    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_pseudo", referencedColumnName="id")
     */
	private $joueur;
    /**
     * @ORM\ManyToOne(targetEntity="Cartes")
     * @ORM\JoinColumn(name="carte1_id", referencedColumnName="id", nullable=true)
     */
	private $carte1;
    /**
     * @ORM\ManyToOne(targetEntity="Cartes")
     * @ORM\JoinColumn(name="carte2_id", referencedColumnName="id", nullable=true)
     */
	private $carte2;


	public function getJoueur(){
		return $this->carte1;
	}
	public function setJoueur($j){
		$this->joueur=$j;
	}
	public function getCarte1(){
		return $this->carte1;
	}
	public function setCarte1($c){
		$this->carte1=$c;
	}
	public function getCarte2(){
		return $this->carte2;
	}
	public function setCarte2($c){
		$this->carte2=c;
	}
}

?>
