<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     private $id;
     /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
     private $price;
     /**
     * @ORM\Column(type="text")
     */
    private $description;

    public function getName(){
    	return $this->name;
    }
    public function setName($n){
    	$this->name=$n;
    }
    public function getPrice(){
    	return $this->price;
    }
    public function setPrice($p){
    	$this->price=$p;
    }
    public function getDescr(){
    	return $this->description;
    }
    public function setDescr($d){
    	$this->description=$d;
    }
}
?>