<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
*@ORM\Table(name="ll_users")
*@ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
*/

class User implements UserInterface, \Serializable{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;

     /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
	private $pseudo;
    /**
     * @ORM\Column(type="string", length=64)
     */
	private $password;
    /**
     *@Assert\NotBlank()
     *@Assert\Length(max=4096)
     */
	private $plainMdp;
    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
	private $email;
    /**
     *@ORM\Column(type="integer")
     */
	private $nbVictoire=0;
    /**
     *@ORM\Column(type="integer")
     */
	private $nbDefaite=0;
    /**
     * @ORM\ManyToOne(targetEntity="Serveur")
     * @ORM\JoinColumn(name="serveur_id", referencedColumnName="id", nullable=true)
     */
	private $serveur;
    /**
     *@ORM\Column(type="integer")
     */
	private $nbPoint=0;	
    /**
     *@ORM\Column(type="integer", nullable=true)
     */
	private $numJoueur;
    public function getId(){
	return $this->id;
    }

    public function getUsername()
    {
        return $this->pseudo;
    }
    public function setUsername($n){
	$this->pseudo=$n;
    }

    public function getSalt()
    {
        return null;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($p){
	$this->password=$p;
    }
    public function getPlainPassword()
    {
        return $this->plainMdp;
    }
    public function setPlainPassword($p){
	$this->plainMdp=$p;
    }
	
    public function getEmail(){
	return $this->email;
    }
    public function setEmail($e){
   	$this->email=$e;
    }
    public function getNbVictoire(){
	return $this->nbVictoire;
    }
    public function setNbVictoire($v){
	$this->nbVictoire=$v;
    }
    public function getNbDefaite(){
	return $this->nbDefaite;
    }
    public function setNbDefaite($d){
	$this->nbDefaite=$d;
    }
    public function getNbPoint(){
	return $this->nbPoint ;
    }
    public function setNbPoint($v){
	$this->nbPoint =$v;
    }
    public function getServeur(){
	return $this->serveur ;
    }
    public function setServeur($v){
	$this->serveur =$v;
    }
    public function getNumJoueur(){
	return $this->numJoueur ;
    }
    public function setNumJoueur($n){
	$this->numJoueur =$n;
    }
    public function getRoles()
    {
        return array('ROLE_USER');
    }
    public function eraseCredentials()
    {
    }
/** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->pseudo,
            $this->password,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->pseudo,
            $this->password,
        ) = unserialize($serialized);
    }
}
?>
