<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
*@ORM\Entity
*@ORM\Table(name="cartes")
*/
class Cartes{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
    /**
     *@ORM\Column(type="integer")
     */
	private $valeur;
    /**
     * @ORM\Column(type="string", length=100)
     */
	private $nom;
    /**
     * @ORM\Column(type="integer",nullable=true)
     */
	private $nbCopies=0;

	public function getValeur(){
		return $this->valeur;
	}
	public function setValeur($v){
		$this->valeur=$v;
	}
	public function getNom(){
		return $this->nom;
	}
	public function setNom($n){
		$this->nom=$n;
	}
	public function getNbCopies(){
		return $this->nbCopies;
	}
	public function setNbCopies($s){
		$this->nbCopies=$s;
	}
}
?>
